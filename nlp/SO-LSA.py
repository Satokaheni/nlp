# -*- coding: utf-8 -*-
import pickle
import sys
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models, similarities, matutils
from collections import defaultdict

ec = sys.stdout.encoding
stemmer = PorterStemmer()
	
joy = ['joy','happy','pleasure', 'enjoy', 'glad', 'amuse'] #joy tags
anger = ['pissed','irritated','annoyed','mad','anger','disappointed'] #anger tags
sadness = ['miserable','unhappy','despair','sorrow','sad','depressed'] #sadness tags
scared = ['terrified','afraid','frightened','scary','fear', 'anxiety'] #scared tags
surprise = ['stunned','amazed','astounded','surprise','shock', 'astonish'] #surprise tags
disgust = ['ew','gross','loathe','nausea','distaste', 'ugly'] #disgust tags

joy = [ stemmer.stem(w) for w in joy ]
anger = [ stemmer.stem(w) for w in anger ]
sadness = [ stemmer.stem(w) for w in sadness ]
scared = [ stemmer.stem(w) for w in scared ]
surprise = [ stemmer.stem(w) for w in surprise ]
disgust = [ stemmer.stem(w) for w in disgust ]

tweet_text = []

#0 = joy, 1 = anger, 2 = sadness, 3 = fear, 4 = surprise, 5 = disgust
emotions = [ 0, 1, 2, 3, 4, 5 ]

#get tweets
with open('tweet_lexicon_corpus.txt', 'rb') as f:
	tweet_text = pickle.load(f)	
	
frequency = defaultdict(int)
for tweet in tweet_text:
	for token in tweet:
		frequency[token] += 1

tweet_text = [[ word for word in tweet if frequency[word] > 1 ] for tweet in tweet_text ]

unigrams = []
for tweet in tweet_text:
	unigrams.extend(tweet)
unigrams = list(set(unigrams))

dictionary = corpora.Dictionary(tweet_text)

corpus = [ dictionary.doc2bow(tweet) for tweet in tweet_text ]

lsi = models.lsimodel.LsiModel(corpus=corpus, id2word=dictionary, num_topics=6)

termcorpus = list(matutils.Dense2Corpus(lsi.projection.u.T))

index = similarities.MatrixSimilarity(termcorpus)

def printsim(query, word):
	sims = index[query]
	fmt = [ (dictionary[idother], sim) for idother,sim in enumerate(sims) ]
	wsim = [ v for w,v in fmt if w == word ]
	if wsim == []:
		return 0
	else:
		return wsim[0]

joy_index = []
anger_index = []
sadness_index = []
scared_index = []
surprise_index = []
disgust_index = []

for i in range(len(dictionary)):
	if dictionary[i] in joy:
		joy_index.append((i,dictionary[i]))
	if dictionary[i] in anger:
		anger_index.append((i,dictionary[i]))
	if dictionary[i] in sadness:
		sadness_index.append((i,dictionary[i]))
	if dictionary[i] in scared:
		scared_index.append((i,dictionary[i]))
	if dictionary[i] in surprise:
		surprise_index.append((i,dictionary[i]))
	if dictionary[i] in disgust:
		disgust_index.append((i,dictionary[i]))

joy_lsa = { w: { ew: 0 for i,ew in joy_index } for w in unigrams }
anger_lsa = { w: { ew: 0 for i,ew in anger_index } for w in unigrams }
sadness_lsa = { w: { ew: 0 for i,ew in sadness_index } for w in unigrams }
scared_lsa = { w: { ew: 0 for i,ew in scared_index } for w in unigrams }
surprise_lsa = { w: { ew: 0 for i,ew in surprise_index } for w in unigrams }
disgust_lsa = { w: {ew: 0 for i,ew in disgust_index } for w in unigrams }

c = 0

print('calculating similarities for words to each emotion')

for w in unigrams:
	for i,ew in joy_index:
		joy_lsa[w][ew] = printsim(termcorpus[i],w)

	for i,ew in anger_index:
		anger_lsa[w][ew] = printsim(termcorpus[i],w)

	for i,ew in sadness_index:
		sadness_lsa[w][ew] = printsim(termcorpus[i],w)

	for i,ew in surprise_index:
		surprise_lsa[w][ew] = printsim(termcorpus[i],w)

	for i,ew in scared_index:
		scared_lsa[w][ew] = printsim(termcorpus[i],w)

	for i,ew in disgust_index:
		disgust_lsa[w][ew] = printsim(termcorpus[i],w)


print('calculating the SO-LSA of each word')

solsa = { w: { e: 0 for e in emotions } for w in unigrams }

for w,e in solsa.items():
	joy_sum = sum(joy_lsa[w].values())
	anger_sum = sum(anger_lsa[w].values())
	sadness_sum = sum(sadness_lsa[w].values())
	surprise_sum = sum(surprise_lsa[w].values())
	scared_sum = sum(scared_lsa[w].values())
	disgust_sum = sum(disgust_lsa[w].values())

	solsa[w][0] = joy_sum
	solsa[w][1] = anger_sum
	solsa[w][2] = sadness_sum
	solsa[w][3] = surprise_sum
	solsa[w][4] = scared_sum
	solsa[w][5] = disgust_sum

		
with open('tweet_lexicon_new.txt', 'wb') as f:
	pickle.dump(solsa, f)
