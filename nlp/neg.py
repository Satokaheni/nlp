import pickle
import re

tweets = []
classes = []
POS = []
correct = []
stemmed = []

with open('tweet_text_pmi.txt', 'rb') as f:
    tweets = pickle.load(f)

with open('tweet_POS_pmi.txt', 'rb') as f:
    POS = pickle.load(f)

with open('tweet_classification_pmi.txt', 'rb') as f:
    classes = pickle.load(f)


neg_tweets = []
neg_class = []
neg_POS = []

#list of negation words
neg = ['hardly','neither','nobody','not','cannot','lack','nor','none','lacking','never','nothing','without','lacks','no','nowhere','didnt','havent','neednt','wasnt','darent','hadnt','isnt','oughtnt','wouldnt','aint','dont','mightnt','shant','cant','doesnt','havnt','mustnt','shouldnt' ]

for tweet in tweets:
    i = 0
    for w in tweet:
        if (w in neg) or (re.search(r'[a-zA-z]+[nN](\')[tT]', w) is not None):
            neg_tweets.append(tweets[i])
            neg_POS.append(POS[i])
            neg_class.append(classes[i])
            break

    i += 1


with open('tweet_text_new.txt', 'ab') as f:
    pickle.dump(neg_tweets, f)

with open('tweet_POS_new.txt', 'ab') as f:
    pickle.dump(neg_POS, f)

with open('tweet_classification_new.txt', 'ab') as f:
    pickle.dump(neg_class, f)
