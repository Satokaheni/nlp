import sys
import unicodedata as ud
import pickle

tweets_text = []
tweets_POS = []
tags_file = open(sys.argv[1], 'r', encoding='utf-8')

#read tags input file
tweet_tag = []
tagged_tweets = []

#remove links since not important
def removeLinks(tag):
	#tag = [ (word,pos) for (word,pos) in tag if pos != 'U' ]
	tag = [ (word,pos) if pos != 'U' else ('link','U') for (word,pos) in tag ]

	return tag

#generalize the names of users to @User
def removeUsers(tag):
	#tag = [ (word,pos) for (word,pos) in tag if pos != '@' ]
	tag = [ (word,pos) if pos != '@' else ('@User', '@') for (word,pos) in tag ]

	return tag

def removeNumerals(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != '$' ]

	return tag

#to keep up with which tweet we are on
count = 0

for line in tags_file:
	if(line != '\n'):
		tweet_tag.append(line)
	else:
		tagged_tweet = []
		for item in tweet_tag:
			split = item.split('\t')
			word, tag = split[0], split[1]
			word = ud.normalize('NFC', word)
			tag = ud.normalize('NFC', tag)
			tagged_tweet.append((word,tag))
		tweet_tag = []
		tagged_tweets.append(tagged_tweet)

tags_file.close()


for tags in tagged_tweets:
	tags = removeLinks(tags)
	tags = removeUsers(tags)
	tags = removeNumerals(tags)
	tweets_text.append([ word for (word,tag) in tags ])
	tweets_POS.append(tags)

with open('tweet_text_lsa.txt', 'wb') as f:
	pickle.dump(tweets_text, f)

with open('tweet_POS_lsa.txt', 'wb') as f:
	pickle.dump(tweets_POS, f)