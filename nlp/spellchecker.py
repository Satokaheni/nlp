import re
import collections

def words(text): return re.findall('[a-z]+', text.lower()) 

def train(features):
    model = collections.defaultdict(lambda: 1)
    for f in features:
        model[f] += 1
    return model
	
def multcheck(w):
	char_list = list(w)
	previous = ''
	count = 1
	index = 0
	remove_list = []
	for c in char_list:
		if c == previous:
			count = count + 1
		if count > 2:
			remove_list.append(index)
		if previous != c:
			count = 1
		previous = c
		index = index + 1
	
	word = []
	index = 0
	for c in char_list:
		if index not in remove_list:
			word.append(c)
		index = index + 1
	
	return ''.join(word)
	
class SpellChecker:
	NWORDS = []
	alphabet = ''

	def __init__(self):
		self.NWORDS = train(words(open('big.txt').read()))
		self.alphabet = 'abcdefghijklmnopqrstuvwxyz'

	def edits1(self, word):
		splits     = [(word[:i], word[i:]) for i in range(len(word) + 1)]
		deletes    = [a + b[1:] for a, b in splits if b]
		transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b)>1]
		replaces   = [a + c + b[1:] for a, b in splits for c in self.alphabet if b]
		inserts    = [a + c + b     for a, b in splits for c in self.alphabet]
		return set(deletes + transposes + replaces + inserts)

	def known_edits2(self, word):
		return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if e2 in self.NWORDS)

	def known(self, words): 
		return set(w for w in words if w in self.NWORDS)

	def correct(self, word):
		candidates = self.known([word]) or self.known(self.edits1(word)) or self.known_edits2(word) or [word]
		return max(candidates, key=self.NWORDS.get)
