# -*- coding: utf-8 -*-
import numpy as np
from numpy import linalg as LA
from scipy.sparse import linalg as SLA
import PosTokenizer as pt
from nltk.util import *
from nltk import probability
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from scipy import sparse
import pickle
import math
import sys
import enchant
import spellchecker
from spellchecker import multcheck
	
def cosAngle(v1, v2):
	magv1 = 0
	magv2 = 0
	dot = np.dot(v1, v2)
	
	for v in v1:
		magv1 = magv1 + v*v
	
	for v in v2:
		magv2 = magv2 + v*v
		
	return (dot/(math.sqrt(magv1)*math.sqrt(magv2)))

stemmer = PorterStemmer()

tweet_text = []
tweet_POS = []
tweet_class = []

#0 = joy, 1 = anger, 2 = sadness, 3 = fear, 4 = surprise, 5 = disgust
emotions = [ 0, 1, 2, 3, 4, 5 ]

#get tweets
with open('tweet_correct_lsa.txt', 'rb') as f:
	tweet_text = pickle.load(f)
	

joy = ['joy','happy','pleasure', 'enjoy', 'glad', 'amuse'] #joy tags
anger = ['pissed','irritated','annoyed','mad','anger','disappointed'] #anger tags
sadness = ['miserable','unhappy','despair','sorrow','sad','depressed'] #sadness tags
scared = ['terrified','afraid','frightened','scary','fear', 'anxiety'] #scared tags
surprise = ['stunned','amazed','astounded','surprise','shock', 'astonish'] #surprise tags
disgust = ['ew','gross','loathe','nausea','distaste', 'ugly'] #disgust tags


unigram_list = []
for tweet in tweet_text:
	unigram_list.extend(tweet)
	
u_count = probability.FreqDist(unigram_list)
unigrams = ([ k for k,v in u_count.items() if v > 3 ])
word_dict = {}
X = sparse.lil_matrix((len(unigrams), len(tweet_text)))

c = 0
print('Creating Matrix X')

for w in unigrams:
	word_count = []
	word_dict[w] = c
	for tweet in tweet_text:
		word_count.append(tweet.count(w))
	
	X[c] = word_count
	if c % 10000 == 0:
		print(c)
	c = c + 1
	
print('Performing Singular Value Decomposition')
kv = min(X.shape[0], X.shape[1]) - 1
U,S,Vt = SLA.svds(X, k=kv, which='LM')
sys.exit(0)
print('Created U S Vt')
X = (U.dot(S)).dot(Vt)

tsolsa = { w: { e: 0.0001 for e in emotions } for w in unigrams }
for w in unigrams:
	for j in joy:
		tsolsa[w][0] = tsolsa[w][0] + cosAngle(X[word_dict[w]], X[word_dict[j]])
	
	for a in anger:
		tsolsa[w][1] = tsolsa[w][1] + cosAngle(X[word_dict[w]], X[word_dict[a]])

	for s in sadness:
		tsolsa[w][2] = tsolsa[w][2] + cosAngle(X[word_dict[w]], X[word_dict[s]])

	for s in scared:
		tsolsa[w][3] = tsolsa[w][3] + cosAngle(X[word_dict[w]], X[word_dict[s]])

	for s in surprise:
		tsolsa[w][4] = tsolsa[w][4] + cosAngle(X[word_dict[w]], X[word_dict[s]])

	for d in disgust:
		tsolsa[w][5] = tsolsa[w][5] + cosAngle(X[word_dict[w]], X[word_dict[d]])		
		
solsa = { w: { e: 0 for e in emotions } for w in unigrams }		
		
for w,v in tsolsa.items():
	for e in emotions:
		ne = 0
		for e2 in emotions:
			if e2 != e:
				ne = ne + tsolsa[w][e2]
				
		solsa[w][e] = tsolsa[w][e] - ne
		
with open('tweet_lexicon.txt', 'ab') as f:
	pickle.dump(solsa, f)
