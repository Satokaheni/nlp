# -*- coding: utf-8 -*-
import pickle
import sys

ec = sys.stdout.encoding

#map emotions to correct number label
def emap(e):
	if e == 'joy':
		return 0
	elif e == 'anger':
		return 1
	elif e == 'sadness':
		return 2
	elif e == 'fear':
		return 3
	elif e == 'surprise':
		return 4
	elif e == 'disgust':
		return 5
	else:
		return -1
		
lex = {}

nrc_lex = open(sys.argv[1], 'r', encoding='utf-8')
nrc_hash = open(sys.argv[2], 'r', encoding='utf-8')

emotions = [0, 1, 2, 3, 4, 5]

for line in nrc_lex:
	
	#get the information of the lexicon
	split = line.split('\t')
	w = split[0]
	emo = split[1]
	s = float(split[2].rstrip('\n'))

	e = emap(emo)

	if e != -1:
		if w not in lex:
			lex[w] = { em: 0 for em in emotions }
		lex[w][e] = s	

for line in nrc_hash:
	
	#get the information
	split = line.split('\t')
	emo = split[0]
	w = split[1]
	s = float(split[2].rstrip('\n'))
	
	e = emap(emo)
	
	if e != -1:
		if w not in lex:
			lex[w] = { em: 0 for em in emotions }
		lex[w][e] = s

with open('tweet_lexicon_new.txt', 'ab') as f:
	pickle.dump(lex, f)
