from nltk.corpus import stopwords

sw = stopwords.words('english')

#tokenize a tweet to get rid of all valid punctuation
def tokenize(pos_tags):
	tweet = []
	#print('Tokenize Tags: ' + str(pos_tags) + '\n\n')
	for w,t in pos_tags:
		if t != ',':
			tweet.append(w.lower())
			
	return tweet
	
#Tokenize a list of sentence
def sentence_tokenize(sentences, pos_tags):
	sentence_tags = []
	start = 0
	for sentence in sentences:
		end = len(sentence.split()) + start
		tags = pos_tags[start:end]
		sentence_tags.append(tokenize(tags))
		start = end
		
	return sentence_tags

def hash_removal(pos_tags):
	tweet = []
	
	for w,t in pos_tags:
		if t != '#':
			tweet.append(w)
	return tweet
