# -*- coding: utf-8 -*-
import numpy as np
import PosTokenizer as pt
import nltk.data
from nltk.stem.porter import PorterStemmer
from nltk.util import *
from nltk import probability
from nltk.corpus import stopwords
from nltk import Tree
from sklearn import svm
from sklearn import cross_validation
from sklearn.grid_search import GridSearchCV
from sklearn.feature_selection import GenericUnivariateSelect, chi2
from scipy import sparse
from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.feature_extraction.text import TfidfTransformer
import chunking
import pickle
import sys
import re
import threading
import time
import enchant
import operator
import string

ec = sys.stdout.encoding
tok = nltk.data.load('tokenizers/punkt/english.pickle')
stemmer = PorterStemmer()
sw = stopwords.words('english')
d = enchant.Dict('en_US')
chunker = chunking.TagChunker()

def keymaxvalue(d):
	v=list(d.values())
	k=list(d.keys())
	return k[v.index(max(v))]

def fixWL(wl,nl):
	nwl = []

	treeFree = False

	while not treeFree:
		for i in range(len(wl)):
			if type(wl[i]) is Tree:
				wl[i] = wl[i][0]

		treeFree = True
		for i in wl:
			if type(i) is Tree:
				treeFree = False


	if len(wl) == 1:
		if wl[0] not in nl:
			if nl[0] == 'like':
				nwl.append(nl[1])
			else:
				nwl.append(nl[0])
		else:
			nwl.append(wl[0])
	else:
		try:
			i = 0
			j = 0
			while i < len(wl) and len(nwl) != len(nl):
				if wl[i] == nl[j]:
					nwl.append(wl[i])
				elif i+1 < len(wl):
					if wl[i]+wl[i+1] in nl[j]:
						start = wl[:i]
						middle = [wl[i]+wl[i+1]]
						if i+2 < len(wl):
							end = wl[i+2:]
						else:
							end = []

						wl = start + middle + end
						i = -1
						j = -1
						nwl = []
					elif wl[i+1] == nl[j][-len(wl[i+1]):]:
						start = wl[:i]
						middle = [wl[i]+wl[i+1]]
						if i+2 < len(wl):
							end = wl[i+2:]
						else:
							end = []

						wl = start + middle + end
						i = -1
						j = -1
						nwl = []
					elif i+2 < len(wl):
						if wl[i+2] == nl[j][-len(wl[i+2]):]:
							start = wl[:i]
							middle = [wl[i] + wl[i+1] + wl[i+2]]
							if i+3 < len(wl):
								end = wl[i+3:]
							else:
								end = []

							wl = start + middle + end
							i = -1
							j = -1
							nwl = []
						else:
							nwl.append(nl[j])
					else:
						nwl.append(nl[j])
				else:
					nwl.append(nl[j])

				i += 1
				j += 1
		except:
			print(wl)
			print(nl)
			print(nwl)

	return nwl


#get whether you want baseline or actual classifier
clf_type = sys.argv[1]
emo_type = sys.argv[2]

intweet_text = [0] * 7
happy_tweets = []
happy_POS = []
anger_tweets = []
anger_POS = []
sadness_tweets = []
sadness_POS = []
fear_tweets = []
fear_POS = []
surprise_tweets = []
surprise_POS = []
disgust_tweets = []
disgust_POS = []
tweet_text = []
tweet_POS = []
tweet_class = []
tweet_correct = []
tweet_lexicons = {}
tweet_stemmed = []
correct_dict = {}

hc = []
ac = []
sadc = []
fc = []
surc = []
dc = []

#get list input from the three text files
with open('tweet_text_happy.txt', 'rb') as f:
	happy_tweets = pickle.load(f)

with open('tweet_POS_happy.txt', 'rb') as f:
	happy_POS = pickle.load(f)

with open('tweet_text_anger.txt', 'rb') as f:
	anger_tweets = pickle.load(f)

with open('tweet_POS_anger.txt', 'rb') as f:
	anger_POS = pickle.load(f)

with open('tweet_text_sadness.txt', 'rb') as f:
	sadness_tweets = pickle.load(f)

with open('tweet_POS_sadness.txt', 'rb') as f:
	sadness_POS = pickle.load(f)

with open('tweet_text_fear.txt', 'rb') as f:
	fear_tweets = pickle.load(f)

with open('tweet_POS_fear.txt', 'rb') as f:
	fear_POS = pickle.load(f)

with open('tweet_text_surprise.txt', 'rb') as f:
	surprise_tweets = pickle.load(f)

with open('tweet_POS_surprise.txt', 'rb') as f:
	surprise_POS = pickle.load(f)

with open('tweet_text_disgust.txt', 'rb') as f:
	disgust_tweets = pickle.load(f)

with open('tweet_POS_disgust.txt', 'rb') as f:
	disgust_POS = pickle.load(f)

tweet_text.extend(happy_tweets)
tweet_text.extend(anger_tweets)
tweet_text.extend(sadness_tweets)
tweet_text.extend(fear_tweets)
tweet_text.extend(surprise_tweets)
tweet_text.extend(disgust_tweets)
tweet_POS.extend(happy_POS)
tweet_POS.extend(anger_POS)
tweet_POS.extend(sadness_POS)
tweet_POS.extend(fear_POS)
tweet_POS.extend(surprise_POS)
tweet_POS.extend(disgust_POS)

if emo_type == 'happy':
	for i in range(len(happy_tweets)):
		tweet_class.append(0)
	for i in range(len(anger_tweets)+len(sadness_tweets)+len(fear_tweets)+len(surprise_tweets)+len(disgust_tweets)):
		tweet_class.append(1)
elif emo_type == 'anger':
	for i in range(len(happy_tweets)):
		tweet_class.append(1)
	for i in range(len(anger_tweets)):
		tweet_class.append(0)
	for i in range(len(sadness_tweets)+len(fear_tweets)+len(surprise_tweets)+len(disgust_tweets)):
		tweet_class.append(1)
elif emo_type == 'sadness':
	for i in range(len(happy_tweets)+len(anger_tweets)):
		tweet_class.append(1)
	for i in range(len(sadness_tweets)):
		tweet_class.append(0)
	for i in range(len(fear_tweets)+len(surprise_tweets)+len(disgust_tweets)):
		tweet_class.append(1)
elif emo_type == 'fear':
	for i in range(len(happy_tweets)+len(anger_tweets)+len(sadness_tweets)):
		tweet_class.append(1)
	for i in range(len(fear_tweets)):
		tweet_class.append(0)
	for i in range(len(surprise_tweets)+len(disgust_tweets)):
		tweet_class.append(1)
elif emo_type == 'surprise':
	for i in range(len(happy_tweets)+len(anger_tweets)+len(sadness_tweets)+len(fear_tweets)):
		tweet_class.append(1)
	for i in range(len(surprise_tweets)):
		tweet_class.append(0)
	for i in range(len(disgust_tweets)):
		tweet_class.append(1)
elif emo_type == 'disgust':
	for i in range(len(happy_tweets)+len(anger_tweets)+len(sadness_tweets)+len(fear_tweets)+len(surprise_tweets)):
		tweet_class.append(1)
	for i in range(len(disgust_tweets)):
		tweet_class.append(0)


#with open('tweet_text_new.txt', 'rb') as f:
#	tweet_text = pickle.load(f)
#
#with open('tweet_POS_new.txt', 'rb') as f:
#	tweet_POS = pickle.load(f)
#
#with open('tweet_classification_new.txt', 'rb') as f:
#	tweet_class = pickle.load(f)

#tweet_class = intweet_text[0] + intweet_text[1] + intweet_text[2] + intweet_text[3] + intweet_text[4] + intweet_text[5] + intweet_text[6]

solsa = {}
nrc = {}
sopmi = {}
nssolsa = {}
nssopmi = {}

with open('tweet_lexicon.txt', 'rb') as f:
	solsa = pickle.load(f)
	sopmi = pickle.load(f)
	nrc = pickle.load(f)
	nssolsa = pickle.load(f)
	nssopmi = pickle.load(f)

tweet_lexicons = dict(sopmi)
tweet_lexicons.update(solsa)
tweet_lexicons.update(nrc)
tweet_lexicons.update(nssolsa)
tweet_lexicons.update(nssopmi)

with open('tweet_correct_new.txt', 'rb') as f:
	tweet_correct = pickle.load(f)


with open('tweet_stemmed_new.txt', 'rb') as f:
	tweet_stemmed = pickle.load(f)

with open('tweet_conv_dict_new.txt', 'rb') as f:
	correct_dict = pickle.load(f)


#list of tags that are punctuation
punc_tags = ['~', ',', 'G']

#list of emotions
emotions = [0, 1, 2, 3, 4, 5]

#list of happy emoticons
happy_emoticons = [':-)', ':)', ':D', ':o)', ':]', ':3', ':c)', ':>', '=]', '8)', '=)', ':}', ':^)', ':っ)', ':-D', '8-D', '8D', 'x-D', 'xD', 'X-D', 'XD', '=-D', '=D', '=-3', '=3', 'B^D', ':-))', ':\'-)', ':\')', '>:P', ':-P', ':P', 'X-P', 'x-p', 'xp', 'XP', ':-p', ':p', '=p', ':-Þ', ':Þ', ':þ', ':-þ', ':-b', ':b', 'd:']

#list of sad emoticons
sad_emoticons = ['>:[', ':-(', ':(', ':-c', ':c', ':-< ', ':っC', ':<', ':-[', ':[', ':{', ':\'-(', ':\'(']

#list of angry emoticons
angry_emoticons = [':-||', ':@', '>:(', '>:\\', '>:/', ':-/', ':-.', ':/', ':\\', '=/', '=\\', ':L', '=L', ':S', '>.<']

#list of surprise emoticons
surp_emoticons = ['>:O', ':-O', ':O', ':-o', ':o', '8-0', 'O_O', 'o-o', 'O_o', 'o_O', 'o_o', 'O-O']

#list of disgust emoticons
disgust_emoticons = ['D:<', 'D:', 'D8', 'D;', 'D=', 'DX', 'v.v', 'D-\':']
	
#list of all possible POS tags
tag_list = ['N', 'O', 'S', '^', 'Z', 'L', 'V', 'A', 'R', '!', 'D', 'P', '&', 'T', 'X', 'Y', '#', 'E', ',', '~', 'G', '@', 'U']

#list of negation words
neg = ['hardly','neither','nobody','not','cannot','lack','nor','none','lacking','never','nothing','without','lacks','no','nowhere','didnt','havent','neednt','wasnt','darent','hadnt','isnt','oughtnt','wouldnt','aint','dont','mightnt','shant','cant','doesnt','havnt','mustnt','shouldnt' ]

#following discourse words
disc_follow = ['but', 'however', 'nevertheless', 'otherwise', 'yet', 'still', 'nonetheless']

#previous discourse words
disc_previous = ['till', 'til', 'despite', 'though', 'although']

#infer discourse words
disc_infer = ['therefore', 'furthermore', 'consequently', 'thus', 'as' 'subsequently', 'eventually', 'hence']

#adverbial comparitives
comp_adv = ['more','less','really','very','better','worse','much']
	
disc = []
disc.extend(disc_follow)
disc.extend(disc_previous)
disc.extend(disc_infer)	
disc.extend(neg)
	
#get list of all possible trigrams, bigrams, trigrams in the tweet corpus
#only use bigrams and trigrams that appear more than three times
#only use unigrams that appear more than 4 times
unigram_list = []
bigrams_list = []
trigrams_list = []

print('Creating ngrams')
		
#remove puctuation
#if clf_type == 'actual':
#	tweets = []
#	for tags in tweet_POS:
#		tweets.append([ w.lower() for (w,t) in tags if t not in punc_tags ])
#	tweet_text = tweets

i = 0

#create ngrams
for tweet in tweet_text:
	#baseline ngrams 
	#if clf_type == 'baseline':
	text = [ w.lower() for w in tweet ]
	#non baseline ngrams no stopwords
	#else:
	#	text = tweet_correct[i]
	unigram_list.extend(text)
	bigrams_list.extend(bigrams(text))
	i += 1


all_unigrams = probability.FreqDist(unigram_list)
unigram_features = [ k for k in all_unigrams.keys() if all_unigrams[k] > 3 ]
all_bigrams = probability.FreqDist(bigrams_list)
bigram_features = [ k for k in all_bigrams.keys() if all_bigrams[k] > 3 ]
print(len(unigram_features))
print(len(tweet_text))

#creating lil_matrix
lil_features = []

if clf_type == 'baseline':
	lil_features = sparse.lil_matrix((len(tweet_text), (len(unigram_features))))# + len(bigram_features))))
else:
	lil_features = sparse.lil_matrix((len(tweet_text), (len(unigram_features) + len(tag_list) + 18)))# + len(tag_list) + 9)))


print('Finished creating ngrams')

#baseline features using bigrams
def feature_baseline(tweet_text, index):
	tweet = [ w.lower() for w,t in tweet_POS[index] ]
	tweet_unigrams = tweet
	tweet_bigrams = bigrams(tweet)
	features = []
	
	#unigram features
	uni_weight = { w: 0 for w in unigram_features }

	#unigram features
	for word in tweet_unigrams:
		if word in unigram_features:
			uni_weight[word] += 1
	
	features.extend([ v for w,v in uni_weight.items() ])

	bi_weight = { w: 0 for w in bigram_features }

	#bigram features
	#for word in tweet_bigrams:
	#	if word in bigram_features:
	#		bi_weight[word] += 1

	#features.extend([ v for w,v in bi_weight.items() ])
	
	lil_features[index, 0:] = features

	if index % 10000 == 0:
		print(index)

	#return np.array(features)

#if you want to run baseline classifier
if clf_type == 'baseline':
	#split data into training and testing
	print('Creating features')		
	#baseline classifier
	for tweet, i in zip(tweet_text, list(range(len(tweet_text)))):
		feature_baseline(tweet,i)

	baseline_features = sparse.csr_matrix(lil_features, dtype=np.float64)
	#baseline_features = csr_matrix([ feature_baseline(tweet) for tweet in tweet_text ], dtype=np.float64)
	baseline_classes = tweet_class
	print('Features Created: ' + str(baseline_features.shape))

	baseline_features, baseline_classes = shuffle(baseline_features, baseline_classes, random_state=0)

#	print('Performing 10-fold cross validation')
#	clf = svm.LinearSVC(C=.1, tol=1.1000000000000001)
#	scores = cross_validation.cross_val_score(clf, baseline_features, baseline_classes, scoring='f1', cv=15)
#	print('F1: %0.5f (+/- %0.5f)' % (scores.mean(), scores.std() * 2))
#	sys.exit(0)

	#print('Testing best parameters for c')

	c_range = [1, .9, .8, .7, .6, .5, .4, .3, .2, .1, .01, .001]
	tol_range = np.arange(1,10,.1)
	param_grid = dict(C=c_range, tol=tol_range)
	grid = GridSearchCV(svm.LinearSVC(), param_grid=param_grid, scoring='f1', cv=15)
	grid.fit(baseline_features, baseline_classes)
	print('The best parameters are %s with a score of %0.5f' % (grid.best_params_, grid.best_score_))
	sys.exit(0)

outside_count = 0
feature_length = 0
neg_words = { w: 0 for w in neg }
neg_count = { e: 0 for e in emotions }
p_tags = {}
neg_tags = {}
neg_tweets = []
error_tree = []
nm = [0] * 2
lc = 0
nlc = 0
#non baseline feature selection
#baseline features using bigrams
def feature_actual(index):
	tweet = [ w.lower() for w in tweet_text[index] ]
	tweet_unigrams = tweet
	tweet_bigrams = bigrams(tweet)
	features = []
	global outside_count
	global tag_count
	global nlc
	global lc
	global nm

	#unigram features
	uni_weight = { w: 0 for w in unigram_features }

	#unigram features
	for word in tweet_unigrams:
		if word in unigram_features:
			uni_weight[word] += 1

	features.extend([ v for w,v in uni_weight.items() ])

	#bi_weight = { w: 0 for w in bigram_features }

	#bigram features
	#for word in tweet_bigrams:
	#	if word in bigram_features:
	#		bi_weight[word] += 1

	#features.extend([ v for w,v in bi_weight.items() ])

	punc = { key: 0 for key in tag_list }
	emoticons = { e: 0 for e in emotions }
	elongated_punc = 0
	elongated_words = 0

	for (w,t) in tweet_POS[index]:
		punc[t] += 1
		if t in punc_tags and len(w) > 1:
			elongated_punc += 1

		if t not in ['#', '@', '~', 'E', '$', ',', 'G']:
			char_list = list(w)
			previous = char_list[0]
			track = {}
			track[previous] = 1
			for i in range(len(char_list)-1):
				if char_list[i+1] == previous:
					track[char_list[i+1]] += 1
				else:
					track[char_list[i+1]] = 1

				previous = char_list[i+1]

			if len([ c for c,v in track.items() if v > 2 ]):
				elongated_words += 1

		if t == 'E':
			if w in happy_emoticons:
				emoticons[0] += 1
			if w in angry_emoticons:
				emoticons[1] += 1
			if w in sad_emoticons:
				emoticons[2] += 1
			if w in surp_emoticons:
				emoticons[4] += 1
			if w in disgust_emoticons:
				emoticons[5] += 1

	del emoticons[3]
	features.extend([ v for w,v in punc.items() ])
	#features.append(elongated_punc)
	features.append(elongated_words)
	#features.append(len([ w for w,t in tweet_POS[index] if w.isupper() ]))
	features.extend([ v for e,v in emoticons.items() ])

	#count number of words associated with each emotion
	wec = { e: 0 for e in emotions }
	total = { e: 0 for e in emotions }
	score = { e: 0 for e in emotions }

	window = 1
	after_neg = False
	i = 0
	k = 0
	text = [ (w.lower(),t) for w,t in tweet_POS[index] if t not in punc_tags ]
	tweet = []
	hash_list = []
	emo_dict = {}
	for w,t in text:
		if (t == 'E') or (w[-1] in string.punctuation):
			tweet.append(('emoticon'+str(k),'E'))
			emo_dict['emoticon'+str(k)] = w
			k += 1
		elif w[0] == '#':
			tweet.append((w[1:],t))
			hash_list.append(i)
		else:
			tweet.append((w,t))
		i += 1

	text = [ w for w,t in tweet ]
	#neg_weight = [ (w,1) for w in text ]
	neg_weight = [ 1 for w in text ]
	wl = []
	i = 0
	#for w in tweet_correct[index]:
	for w,t in tweet:
		if (w.lower() in neg) or (re.search(r'[a-zA-z]+[nN](\')[tT]', w) is not None):
			outside_count += 1
			neg_count[tweet_class[index]] += 1
			after_neg = True
		elif after_neg and window == 0:
			after_neg = False
			window = 5
		elif after_neg: # and w not in prev and w not in follow and w not in inf:
			try:
				if t == 'R':
					if not (w in comp_adv or w[-2:] == 'er'):
						neg_weight[i] *= -1 #(w,-1)
				elif t == 'A':
					neg_weight[i] *= -1 #(w,-1)
				elif t == '&':
					pass
				elif w == 'like':
					nlc += 1
					if i+1 < len(tweet):
						pt = chunker.parse(text[i+1:])
						l,wl = chunker.phrase(pt[0],tweet[i+1][1])
						if l == 'ERROR':
							lc += 1
							neg_weight[i] = -1 #(w,-1)
						else:
							wl.insert(0,'like')
							wl = fixWL(wl,text[i:])
							for v in range(len(wl)):
								neg_weight[i+v] *= -1 #(wl[v], -1)
						if l not in p_tags:
							p_tags[l] = 0
						p_tags[l] += 1
					else:
						neg_weight[i] *= -1 #(w,-1)
				else:
					pt = chunker.parse(text[i:])
					l,wl = chunker.phrase(pt[0],t)
					if l == 'ERROR':
						neg_weight[i] *= -1 #(w,-1)
					else:
						wl = fixWL(wl,text[i:])
						for v in range(len(wl)):
							neg_weight[i+v] *= -1 #(wl[v], -1)
					if l not in p_tags:
						p_tags[l] = 0
					p_tags[l] += 1
			except StopIteration:
				neg_weight[i] *= -1 #(w,-1)
			#	print(text)
			#	print(text[i:])
			#	print(wl)
			#	print(neg_weight)
			#	print(len(wl))
			#	print((text[i]))
			#	print(i)
			#	print(t)
			#	print(index)
			#	sys.exit(0)
			#	neg_tweets.append(index)
			break


		i += 1

	#converting back emoticons, hashtags
	tweet_conv = []
	try:
		for i in range(len(text)):
			if 'emoticon' == text[i][:8] and text[i][-1].isdigit():
				tweet_conv.append(emo_dict[text[i]])
			elif i in hash_list:
				tweet_conv.append('#'+text[i])
			else:
				tweet_conv.append(text[i])
	except KeyError:
		print(tweet_conv)
		print(text)
		print(emo_dict)
		print(hash_list)
		sys.exit(0)

	tweet = []
	neg_list = []
	#converting to correctly spelled word
	for i in range(len(tweet_conv)):
		try:
			nw = correct_dict[tweet_conv[i]].split()
			tweet.extend(nw)
			for j in range(len(nw)):
				neg_list.append(neg_weight[i])

		except:
			print(tweet_correct[index])
			print(tweet_conv)
			print(tweet_text[index])
			print(tweet)
			sys.exit(0)

	#calculating score, wec and total
	i = 0
	for w in tweet: #tweet_correct[index]:
		if w in tweet_lexicons:
			ew = { e: neg_list[i]*v for e,v in tweet_lexicons[w].items() }
			ei = keymaxvalue(ew) #tweet_lexicons[w])
			wec[ei] += 1
			for e in emotions:
				score[e] += ew[e] #tweet_lexicons[w][e]
				if ew[e] > 0: #tweet_lexicons[w][e] > 0:
					total[e] += 1
		i += 1

	features.extend([ v for e,v in wec.items() ])
	#features.extend([ v for e,v in total.items() ])
	features.extend([ abs(v) for e,v in score.items() ])
	#neg_weight = { w: 1 for w in tweet_correct[index] }

	lil_features[index, 0:] = features

	if index % 10000 == 0:
		print(index)

#actual classifier
#actual_features = [ features(tweet,tag,punc) for (tweet,tag,punc) in zip(tweet_text, tweet_POS, punc_tweet) ]
#for (tweet,tag,punc,correct,index) in zip(tweet_text, tweet_POS, punc_tweet, tweet_correct, list(range(len(tweet_text)))):
#	features(tweet,tag,correct,punc,index)
#split data into training and testing
#print('Creating features')
#baseline classifier
for i in list(range(len(tweet_text))):
	feature_actual(i)

actual_features = sparse.csr_matrix(lil_features, dtype=np.float64)
#baseline_features = csr_matrix([ feature_baseline(tweet) for tweet in tweet_text ], dtype=np.float64)
actual_classes = tweet_class
actual_features = GenericUnivariateSelect(chi2, mode='percentile', param=85).fit_transform(actual_features, actual_classes)
#actual_features = TfidfTransformer().fit_transform(actual_features, actual_classes)
print('Features Created: ' + str(actual_features.shape))

actual_features, actual_classes = shuffle(actual_features, actual_classes, random_state=0)

print('Performing 10-fold cross validation')
#clf = svm.LinearSVC(C=.1)
#scores = cross_validation.cross_val_score(clf, baseline_features, baseline_classes, cv=10)
#print('Baseline Accuracy: %0.5f (+/- %0.5f)' % (scores.mean(), scores.std() * 2))
#sys.exit(0)

#print('Testing best parameters for c')

#c_range = [1, .9, .8, .7, .6, .5, .4, .3, .2, .1, .01, .001]
#tol_range = np.arange(1,10,.1)
#param_grid = dict(C=c_range, tol=tol_range)
#grid = GridSearchCV(svm.LinearSVC(), param_grid=param_grid, cv=10)
#grid.fit(actual_features, actual_classes)
#print('The best parameters are %s with a score of %0.5f' % (grid.best_params_, grid.best_score_))
#for index in range(len(tweet_text)):
#	feature_actual(index)
#actual_features = sparse.csr_matrix(lil_features, dtype=np.float64)

#actual_features, tweet_class = shuffle(actual_features, tweet_class, random_state=0)

#10-fold cross validation
#print('Testing Classifier using 10-fold cross validation')

print('Number of negations: ' + str(outside_count/len(tweet_text)))
#print(p_tags)
#print(nm)
#print(str(nlc) + ':' + str(lc))
#with open('error_tree.txt', 'wb') as f:
#	pickle.dump(error_tree,f)
clf = svm.LinearSVC(C=.1, tol=1.1000000000000001)
scores = cross_validation.cross_val_score(clf, actual_features, actual_classes, scoring='f1_weighted', cv=15)
print('F1: %0.5f, (+/-) %0.5f' % ((scores.mean(), 2*scores.std()))) # + scores.std() * 2)))
sys.exit(0)

c_range = [1, .9, .8, .7, .6, .5, .4, .3, .2, .1, .01, .001, .0001, .00001]
tol_range = np.arange(1,2,.1)
param_grid = dict(C=c_range, tol=tol_range)
grid = GridSearchCV(svm.LinearSVC(), param_grid=param_grid, cv=10)
grid.fit(actual_features, actual_classes)
print('The best parameters are %s with a score of %0.5f' % (grid.best_params_, grid.best_score_))
