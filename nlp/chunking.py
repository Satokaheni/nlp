import nltk.chunk
import nltk.corpus
import nltk.tag
import pickle
import sys
import os
import re
from nltk.parse import stanford
from nltk import Tree

def changePOS(s):
    new_pos = []
    for w, t in s:
        if t == 'N' or t == 'S' or t == 'O' or t == '^' or t == 'Z':
            new_pos.append((w, 'NN'))
        if t == 'M':
            s = w.split("'")
            new_pos.append((s[0],'NN'))
            new_pos.append(("'"+s[1], 'VB'))
        #elif t == 'O':
        #    new_pos.append((w, 'PRP'))
        #elif t == '^' or t == 'Z':
        #    new_pos.append((w, 'NNP'))
        elif t == 'V':
            new_pos.append((w, 'VB'))
        elif t == 'A':
            new_pos.append((w, 'JJ'))
        elif t == 'R':
            new_pos.append((w, 'RB'))
        elif t == '!':
            new_pos.append((w, 'UH'))
        elif t == 'D':
            new_pos.append((w, 'DT'))
        elif t == 'P':
            new_pos.append((w, 'IN'))
        elif t == '&':
            new_pos.append((w, 'CC'))
        elif t == 'T':
            new_pos.append((w, 'RP'))
        elif t == 'X' or t == 'Y':
            new_pos.append((w, 'EX'))

    return new_pos


class TagChunker(nltk.chunk.ChunkParserI):

    phrase_tags = ['ADJP','ADVP','NP','VP','PRT','QP','NAC','NX','PP','PRN','RRC','UCP','WHADJP','WHAVP','WHNP','WHPP','CONJP','SYM']
    noun_tags = ['N','O','S','^','Z','L','M','DT']
    start_tags = ['SBAR','S','ROOT','FRAG','SBARQ','SINV','SQ', 'X', 'CC']

    def __init__(self):
        os.environ['STANFORD_PARSER'] = 'jars'
        os.environ['STANFORD_MODELS'] = 'jars'
        self.parser = stanford.StanfordParser(model_path='jars/englishPCFG.ser.gz')

    def np(self,tree):
        try:
            if tree.label() in ['NP','WHNP']:
                return tree
            else:
                for i in range(len(tree)):
                    t = self.np(tree[i])
                    if t.label() in ['NP','WHNP']:
                        return t

                return Tree('NP',['ERROR'])
        except:
            return Tree('NP',['ERROR'])

    def vp(self,tree):
        verb_phrases = ['VP','VBD','VB','VBP','VBZ','VBG','VBN']
        try:
            if tree.label() in verb_phrases:
                return tree
            else:
                for i in range(len(tree)):
                    t = self.vp(tree[i])
                    if t.label() in verb_phrases:
                        return t

                return Tree('VP',['ERROR'])
        except:
            return Tree('VP',['ERROR'])

    def pp(self,tree):
        try:
            if tree.label() in ['PP','TO','IN']:
                return tree
            else:
                for i in range(len(tree)):
                    t = self.pp(tree[i])
                    if t.label() in ['PP','TO','IN']:
                        return t

                return Tree('PP',['ERROR'])
        except:
            return Tree('PP',['ERROR'])

    def wordlist(self,tree):
        try:
            if tree.label() not in self.phrase_tags and tree.label() not in self.start_tags:
                if type(tree[0]) is Tree:
                    return tree[0]
                else:
                    return tree
            else:
                wl = []
                for i in range(len(tree)):
                    wl.extend(self.wordlist(tree[i]))
                return wl
        except:
            return [tree]

    def parse(self, tokens):
        return list(self.parser.raw_parse(' '.join(tokens)))

    def parseTag(self, tokens):
        tokens = changePOS(tokens)
        return list(self.parser.tagged_parse(tokens))

    def phrase(self, tree, tag):
        try:
            while tree.label() in self.start_tags:
                tree = tree[0]
        except:
            return 'ERROR',[tree]

        if tag in self.noun_tags:
            tree = self.np(tree)
        elif tag in ['V','T']:
            tree = self.vp(tree)
        elif tag == 'P':
            tree = self.pp(tree)

        if tree[0] == 'ERROR':
            return 'ERROR',[]
        else:
            l = tree.label()
            wl = []
            wl.extend(self.wordlist(tree))

            isTree = True

            while isTree:
                isTree = False
                for i in range(len(wl)):
                    if type(wl[i]) is Tree:
                        isTree = True
                        start = wl[:i]
                        if i+1 < len(wl):
                            end = wl[i+1:]
                        else:
                            end = []

                        middle = []
                        middle.extend(self.wordlist(wl[i]))

                        wl = start + middle + end

            return l,wl
