import pickle
import string
from nltk import Tree

et = []

with open('error_tree.txt', 'rb') as f:
    et = pickle.load(f)

c = 0

#for i in et:
#    print(i)
#    print('\n')
#    c += 1

print(c)

def fixWL(wl,nl):
    nwl = []

    treeFree = False

    while not treeFree:
        for i in range(len(wl)):
            if type(wl[i]) is Tree:
                print(wl[i])
                wl[i] = wl[i][0]

        treeFree = True
        for i in wl:
            if type(i) is Tree:
                treeFree = False

    if len(wl) == 1:
        if wl[0] not in nl:
            if nl[0] == 'like':
                nwl.append(nl[1])
            else:
                nwl.append(nl[0])
        else:
            nwl.append(wl[0])
    else:
        i = 0
        j = 0
        print(wl)
        print(nl)
        while i < len(wl) and len(nwl) != len(nl):
            print(str(i) + ' ' + str(len(wl)) + ' ' + str(j) + ' ' + str(len(nl)))
            if wl[i] == nl[j]:
                nwl.append(wl[i])
            elif i+1 < len(wl):
                if wl[i]+wl[i+1] in nl[j]:
                    start = wl[:i]
                    middle = [wl[i]+wl[i+1]]
                    if i+2 < len(wl):
                        end = wl[i+2:]
                    else:
                        end = []

                    wl = start + middle + end
                    i = -1
                    j = -1
                    nwl = []
                elif wl[i+1] == nl[j][-len(wl[i+1]):]:
                    start = wl[:i]
                    middle = [wl[i]+wl[i+1]]
                    if i+2 < len(wl):
                        end = wl[i+2:]
                    else:
                        end = []

                    wl = start + middle + end
                    i = -1
                    j = -1
                    nwl = []
                elif i+2 < len(wl):
                    if wl[i+2] == nl[j][-len(wl[i+2]):]:
                        start = wl[:i]
                        middle = [wl[i] + wl[i+1] + wl[i+2]]
                        if i+3 < len(wl):
                            end = wl[i+3:]
                        else:
                            end = []

                        wl = start + middle + end
                        i = -1
                        j = -1
                        nwl = []
                    else:
                        nwl.append(nl[j])
                else:
                    nwl.append(nl[j])
            else:
                nwl.append(nl[j])

            i += 1
            j += 1

    print(nl)
    print(wl)
    return nwl



 #               if wl[i]+wl[i+1] in nl[j] and len(wl[i]+wl[i+1] == len(nl[j])):
  #                  print('if: ' + wl[i] + ' ' + wl[i+1] + ' ' + nl[j])
   #                 nwl.append(nl[j])
    #                i += 1
#                if wl[i+1] == nl[j][-len(wl[i+1]):]:
#                    print('elif: ' + wl[i] + ' ' + wl[i+1] + ' ' + nl[j])
#                    nwl.append(nl[j])
#                    i += 1
#                elif i+2 < len(wl):
#                    if wl[i+2] == nl[j][-len(wl[i+2]):]:
#                        print('elif if: ' + wl[i] + ' ' + wl[i+2] + ' ' + nl[j])
#                        nwl.append(nl[j])
#                        i += 2
#                    else:
#                        print('elif else: ' + wl[i] + ' ' + wl[i+2] + ' ' + nl[j])
#                        nwl.append(nl[j])
#                else:
#                    print('if else: ' + wl[i] + ' ' + wl[i+1] + ' ' + nl[j])
#                    nwl.append(nl[j])
#            elif i+1 >= len(wl):
#                print('else: ' + wl[i] + ' ' + nl[j])
#                nwl.append(nl[j])
#
#            j += 1
#            i += 1

#    print(wl)
#    print(nl)

#    return nwl

print(fixWL(['help', 'much', 'i', "'m", 'a', 'bit', 'of', 'a', 'wuss', 'with', 'thunder', 'and', '⚡', '⚡', '⚡'], ['help', 'much', "i'm", 'a', 'bit', 'of', 'a', 'wuss', 'with', 'thunder', 'and', '⚡️⚡️⚡️']))