# -*- coding: utf-8 -*-
import numpy as np
import PosTokenizer as pt
import nltk.data
from nltk.stem.porter import PorterStemmer
from nltk.util import *
from nltk import probability
from nltk.corpus import stopwords
from nltk import Tree
from sklearn import svm
from sklearn import cross_validation
from sklearn.grid_search import GridSearchCV
from sklearn.feature_selection import GenericUnivariateSelect, chi2, SelectPercentile
from scipy import sparse
from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.metrics import precision_recall_fscore_support, classification_report
import chunking
import pickle
import sys
import re
import threading
import time
import enchant
import operator
import string
import clf

def getEmo(e):
    if e == 1:
        return 'happy'
    if e == 2:
        return 'anger'
    if e == 3:
        return 'sadness'
    if e == 4:
        return 'fear'
    if e == 5:
        return 'surprise'
    if e == 6:
        return 'disgust'

def prf(pred,c):
    tp = 0
    fp = 0
    fn = 0
    i = 0

    for p in pred:
        if p == 0 and c[i] == 0:
            tp += 1
        elif p != 0 and c[i] == 0:
            fn += 1
        elif p == 0 and c[i] != 0:
            fp += 1
        i += 1

    try:
        precision = tp/(tp+fp)
        recall = tp/(tp+fn)
        f1 = 2*((precision*recall)/(precision+recall))

    except:
        print(tp)
        print(fn)
        print(fp)
        sys.exit(0)

    return precision,recall,f1

def avg(score_list):
    mean_sum = 0
    mean_std = 0
    for p,m,s in score_list:
        if m > mean_sum:
            mean_sum = m
        if s.std() > mean_std:
            mean_std = s.std()

    return mean_sum, mean_std

def getSamples(preds, classes, tweets, true_classes):
    correct = []
    wrong = []

    for i in range(len(preds)):
        if preds[i] == classes[i]:
            correct.append((' '.join(tweets[i])) + '\nactual: ' + str(getEmo(true_classes[i])) + '\tpredicted: ' + str(preds[i]))
        else:
            wrong.append((' '.join(tweets[i])) + '\nactual: ' + str(getEmo(true_classes[i])) + '\tpredicted: ' + str(preds[i]))

    return correct,wrong

happy_tweets = []
happy_POS = []
anger_tweets = []
anger_POS = []
sadness_tweets = []
adness_POS = []
fear_tweets = []
fear_POS = []
surprise_tweets = []
surprise_POS = []
disgust_tweets = []
disgust_POS = []
tweet_text = []
tweet_POS = []
tweet_class = []
tweet_correct = []
tweet_lexicons = {}
tweet_stemmed = []
correct_dict = {}

with open('tweet_text_happy.txt', 'rb') as f:
	happy_tweets = pickle.load(f)

with open('tweet_text_anger.txt', 'rb') as f:
	anger_tweets = pickle.load(f)

with open('tweet_text_sadness.txt', 'rb') as f:
	sadness_tweets = pickle.load(f)

with open('tweet_text_fear.txt', 'rb') as f:
	fear_tweets = pickle.load(f)

with open('tweet_text_surprise.txt', 'rb') as f:
	surprise_tweets = pickle.load(f)

with open('tweet_text_disgust.txt', 'rb') as f:
	disgust_tweets = pickle.load(f)

tweet_text.extend(happy_tweets)
tweet_text.extend(anger_tweets)
tweet_text.extend(sadness_tweets)
tweet_text.extend(fear_tweets)
tweet_text.extend(surprise_tweets)
tweet_text.extend(disgust_tweets)

happy_class = []
for i in range(len(happy_tweets)):
    happy_class.append(1.0)
    tweet_class.append(1.0)
for i in range(len(anger_tweets)+len(sadness_tweets)+len(fear_tweets)+len(surprise_tweets)+len(disgust_tweets)):
    happy_class.append(-1.0)
anger_class = []
for i in range(len(happy_tweets)):
    anger_class.append(-1.0)
for i in range(len(anger_tweets)):
    anger_class.append(1.0)
    tweet_class.append(2.0)
for i in range(len(sadness_tweets)+len(fear_tweets)+len(surprise_tweets)+len(disgust_tweets)):
    anger_class.append(-1.0)
sadness_class = []
for i in range(len(happy_tweets)+len(anger_tweets)):
    sadness_class.append(-1.0)
for i in range(len(sadness_tweets)):
    sadness_class.append(1.0)
    tweet_class.append(3.0)
for i in range(len(fear_tweets)+len(surprise_tweets)+len(disgust_tweets)):
    sadness_class.append(-1.0)
fear_class = []
for i in range(len(happy_tweets)+len(anger_tweets)+len(sadness_tweets)):
    fear_class.append(-1.0)
for i in range(len(fear_tweets)):
    fear_class.append(1.0)
    tweet_class.append(4.0)
for i in range(len(surprise_tweets)+len(disgust_tweets)):
    fear_class.append(-1.0)
surprise_class = []
for i in range(len(happy_tweets)+len(anger_tweets)+len(sadness_tweets)+len(fear_tweets)):
    surprise_class.append(-1.0)
for i in range(len(surprise_tweets)):
    surprise_class.append(1.0)
    tweet_class.append(5.0)
for i in range(len(disgust_tweets)):
    surprise_class.append(-1.0)
disgust_class = []
for i in range(len(happy_tweets)+len(anger_tweets)+len(sadness_tweets)+len(fear_tweets)+len(surprise_tweets)):
    disgust_class.append(-1.0)
for i in range(len(disgust_tweets)):
    disgust_class.append(1.0)
    tweet_class.append(6.0)


print('Creating features')

for i in list(range(len(tweet_text))):
	clf.feature_actual(i)

lil_features = clf.lil_features

actual_features = sparse.csr_matrix(lil_features, dtype=np.float64)

#actual_features, actual_classes = shuffle(actual_features, tweet_class, random_state=0)

print('Performing 10-fold Cross Validation')

#c_range = [1, .9, .8, .7, .6, .5, .4, .3, .2, .1, .01, .001]
#param_grid = dict(C=c_range)
#grid = GridSearchCV(svm.LinearSVC(), param_grid=param_grid, scoring='precision_weighted', cv=10)
#grid.fit(actual_features, actual_classes)
#print(avg(grid.grid_scores_))
#print('The best parameters are %s with a score of %0.5f' % (grid.best_params_, grid.best_score_))

#clf = svm.LinearSVC()
#actual_scores = cross_validation.cross_val_score(clf, actual_features, tweet_class, scoring='precision', cv=10)
#print('Actual F1: %0.5f /%0.5f' % ((actual_scores.mean(), actual_scores.std())))#, (actual_scores.mean() + 2*actual_scores.std())))

actual_features, tweet_class, happy_class, anger_class, sadness_class, fear_class, surprise_class, disgust_class, tweet_text = shuffle(actual_features, tweet_class, happy_class, anger_class, sadness_class, fear_class, surprise_class, disgust_class, tweet_text, random_state=0)

#all_features = SelectPercentile(chi2, percentile=85).fit_transform(actual_features, tweet_class)
#happy_features = GenericUnivariateSelect(chi2, mode='percentile', param=85).fit_transform(actual_features, happy_class)
#anger_features = GenericUnivariateSelect(chi2, mode='percentile', param=85).fit_transform(actual_features, anger_class)
#sadness_features = GenericUnivariateSelect(chi2, mode='percentile', param=85).fit_transform(actual_features, sadness_class)
#fear_features = GenericUnivariateSelect(chi2, mode='percentile', param=85).fit_transform(actual_features, fear_class)
#surprise_features = GenericUnivariateSelect(chi2, mode='percentile', param=85).fit_transform(actual_features, surprise_class)
#disgust_features = GenericUnivariateSelect(chi2, mode='percentile', param=85).fit_transform(actual_features, disgust_class)

clf = svm.LinearSVC(C=.1, tol=1.1000000000000001)
#clf = svm.LinearSVC(C=.1, tol=1.1000000000000001)

#actual_preds = cross_validation.cross_val_predict(clf, actual_features, tweet_class, cv=10)
#correct, wrong = getSamples(actual_preds, tweet_class, tweet_text)
happy_scores = cross_validation.cross_val_predict(clf, actual_features, happy_class, cv=10)
anger_scores = cross_validation.cross_val_predict(clf, actual_features, anger_class, cv=10)
sadness_scores = cross_validation.cross_val_predict(clf, actual_features, sadness_class, cv=10)
fear_scores = cross_validation.cross_val_predict(clf, actual_features, fear_class, cv=10)
surprise_scores = cross_validation.cross_val_predict(clf, actual_features, surprise_class, cv=10)
disgust_scores = cross_validation.cross_val_predict(clf, actual_features, disgust_class, cv=10)

hc, hw = getSamples(happy_scores, happy_class, tweet_text, tweet_class)
ac, aw = getSamples(anger_scores, anger_class, tweet_text, tweet_class)
sac, saw = getSamples(sadness_scores, sadness_class, tweet_text, tweet_class)
fc, fw = getSamples(fear_scores, fear_class, tweet_text, tweet_class)
suc, suw = getSamples(surprise_scores, surprise_class, tweet_text, tweet_class)
dc, dw = getSamples(disgust_scores, disgust_class, tweet_text, tweet_class)
with open('samples_neg.txt', 'wb') as f:
    pickle.dump(hc,f)
    pickle.dump(hw,f)
    pickle.dump(ac,f)
    pickle.dump(aw,f)
    pickle.dump(sac,f)
    pickle.dump(saw,f)
    pickle.dump(fc,f)
    pickle.dump(fw,f)
    pickle.dump(suc,f)
    pickle.dump(suw,f)
    pickle.dump(dc,f)
    pickle.dump(dw,f)

sys.exit(0)

#happy = list(precision_recall_fscore_support(happy_class,happy_scores,average='binary'))
#anger = list(precision_recall_fscore_support(anger_class,anger_scores,average='binary'))
#sadness = list(precision_recall_fscore_support(sadness_class,sadness_scores,average='binary'))
#fear = list(precision_recall_fscore_support(fear_class,fear_scores,average='binary'))
#surprise = list(precision_recall_fscore_support(surprise_class,surprise_scores,average='binary'))
#disgust = list(precision_recall_fscore_support(disgust_class,disgust_scores,average='binary'))


actual_scores = cross_validation.cross_val_score(clf, actual_features, tweet_class, scoring='f1_weighted', cv=10)
happy_scores = cross_validation.cross_val_score(clf, happy_features, happy_class, scoring='f1_weighted', cv=10)
anger_scores = cross_validation.cross_val_score(clf, anger_features, anger_class, scoring='f1_weighted', cv=10)
sadness_scores = cross_validation.cross_val_score(clf, sadness_features, sadness_class, scoring='f1_weighted', cv=10)
fear_scores = cross_validation.cross_val_score(clf, fear_features, fear_class, scoring='f1_weighted', cv=10)
surprise_scores = cross_validation.cross_val_score(clf, surprise_features, surprise_class, scoring='f1_weighted', cv=10)
disgust_scores = cross_validation.cross_val_score(clf, disgust_features, disgust_class, scoring='f1_weighted', cv=10)

print('Actual F1: %0.5f, +%0.5f' % ((actual_scores.mean(), actual_scores.std())))
print('Joy F1: %0.5f, +%0.5f' % ((happy_scores.mean(), happy_scores.std())))
#print('Joy: ' + str(happy))
print('Anger F1: %0.5f, +%0.5f' % (anger_scores.mean(), anger_scores.std()))
#print('Anger: ' + str(anger))
print('Sadness F1: %0.5f, +%0.5f' % (sadness_scores.mean(), sadness_scores.std()))
#print('Sadness: ' + str(sadness))
print('Fear F1: %0.5f, +%0.5f' % (fear_scores.mean(), fear_scores.std()))
#print('Fear: ' + str(fear))
print('Surprise F1: %0.5f, +%0.5f' % (surprise_scores.mean(), surprise_scores.std()))
#print('Surprise: ' + str(surprise))
print('Disgust F1: %0.5f, +%0.5f' % (disgust_scores.mean(), disgust_scores.std()))
#print('Disgust: ' + str(disgust))


#with open('samples.txt', 'wb') as f:
#    pickle.dump(correct[50:], f)
#    pickle.dump(wrong[50:], f)

