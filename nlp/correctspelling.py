# -*- coding: utf-8 -*-
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import pickle
import enchant
import spellchecker
from spellchecker import multcheck

stemmer = PorterStemmer()

intweet = [None] * 6
tweet_text = []
tweet_POS = []
tweet_class = []

#0 = joy, 1 = anger, 2 = sadness, 3 = fear, 4 = surprise, 5 = disgust
emotions = [ 0, 1, 2, 3, 4, 5 ]

with open('tweet_POS_new.txt', 'rb') as f:
	tweet_POS = pickle.load(f)
#	intweet[0] = pickle.load(f)
#	intweet[1] = pickle.load(f)
#	intweet[2] = pickle.load(f)
#	intweet[3] = pickle.load(f)
#	intweet[4] = pickle.load(f)
#	intweet[5] = pickle.load(f)

#tweet_POS = intweet[0] + intweet[1] + intweet[2] + intweet[3] + intweet[4] + intweet[5]

tweets = []
stemmed_tweets = []
sw = stopwords.words('english')
sc = spellchecker.SpellChecker()
d = enchant.Dict('en_US')
print(len(tweet_POS))
c = 0

conv_dict = {}

for tags in tweet_POS:
	#remove punctuation
	try:
		tweet = [ (w,t) for w,t in tags if t != ',' and t != 'G' and t != '~' ]
	except ValueError:
		print(tags)
	tweet = [ (w,t) for w,t in tweet if w.lower() ]
	text = []
	for w,t in tweet:
		ow = w
		if t == '#' or t == '$' or t == 'E' or t == '^' or t == 'U' or t == '@':
			text.append(w)
		elif d.check(w.lower()) or d.check(w):
			text.append(w)
		else:
			w = sc.correct(multcheck(w.lower()))
			if d.check(w):
				text.append(w)
		if ow not in conv_dict.keys():
			conv_dict[ow.lower()] = w.lower()

	text = [ w.lower() for w in text ]
	stemmed_tweets.append([ stemmer.stem(w) for w in text ])
	tweets.append(text)

	if c % 10000 == 0:
		print(c)
	c = c + 1

print(len(tweet_POS))
print(len(tweets))
with open('tweet_correct_new.txt', 'wb') as f:
	pickle.dump(tweets, f)


with open('tweet_stemmed_new.txt', 'wb') as f:
	pickle.dump(stemmed_tweets, f)

with open('tweet_conv_dict.txt', 'wb') as f:
	pickle.dump(conv_dict, f)