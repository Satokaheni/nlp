import pickle
import re
import sys

correct_non = []
wrong_non = []

with open('samples_no.txt', 'rb') as f:
    correct_non.append(pickle.load(f))
    wrong_non.append(pickle.load(f))
    correct_non.append(pickle.load(f))
    wrong_non.append(pickle.load(f))
    correct_non.append(pickle.load(f))
    wrong_non.append(pickle.load(f))
    correct_non.append(pickle.load(f))
    wrong_non.append(pickle.load(f))
    correct_non.append(pickle.load(f))
    wrong_non.append(pickle.load(f))
    correct_non.append(pickle.load(f))
    wrong_non.append(pickle.load(f))


correct_neg = []
wrong_neg = []

with open('samples_no.txt', 'rb') as f:
    correct_neg.append(pickle.load(f))
    wrong_neg.append(pickle.load(f))
    correct_neg.append(pickle.load(f))
    wrong_neg.append(pickle.load(f))
    correct_neg.append(pickle.load(f))
    wrong_neg.append(pickle.load(f))
    correct_neg.append(pickle.load(f))
    wrong_neg.append(pickle.load(f))
    correct_neg.append(pickle.load(f))
    wrong_neg.append(pickle.load(f))
    correct_neg.append(pickle.load(f))
    wrong_neg.append(pickle.load(f))

neg = ['hardly','neither','nobody','not','cannot','lack','nor','none','lacking','never','nothing','without','lacks','no','nowhere','didnt','havent','neednt','wasnt','darent','hadnt','isnt','oughtnt','wouldnt','aint','dont','mightnt','shant','cant','doesnt','havnt','mustnt','shouldnt' ]

correct = list(range(6))

wrong = list(range(6))

for i in range(len(correct_neg)):
    negation = []
    for tweet in correct_neg[i]:
        t = tweet.split('\n')
        for w in t[0].split():
                if (w.lower() in neg) or (re.search(r'[a-zA-z]+[nN](\')[tT]', w) is not None):
                    negation.append(tweet)
                    break
    correct_neg[i] = negation


for i in range(len(correct_non)):
    negation = []
    for tweet in correct_non[i]:
        t = tweet.split('\n')
        for w in t[0].split():
                if (w.lower() in neg) or (re.search(r'[a-zA-z]+[nN](\')[tT]', w) is not None):
                    negation.append(tweet)
                    break
    correct_non[i] = negation


for i in range(len(wrong_neg)):
    negation = []
    for tweet in wrong_neg[i]:
        t = tweet.split('\n')
        for w in t[0].split():
                if (w.lower() in neg) or (re.search(r'[a-zA-z]+[nN](\')[tT]', w) is not None):
                    negation.append(tweet)
                    break
    wrong_neg[i] = negation


for i in range(len(wrong_non)):
    negation = []
    for tweet in wrong_non[i]:
        t = tweet.split('\n')
        for w in t[0].split():
                if (w.lower() in neg) or (re.search(r'[a-zA-z]+[nN](\')[tT]', w) is not None):
                    negation.append(tweet)
                    break
    wrong_non[i] = negation

print('Happy:')
print('Correct')
for i in range(10):
    print(correct_neg[0][i])
print('\nWrong')
for i in range(10):
    print(wrong_non[0][i])

print('\nAnger:')
print('Correct')
for i in range(10):
    print(correct_neg[1][i])
print('\nWrong')
for i in range(10):
    print(wrong_non[1][i])


print('\nSadness:')
print('Correct')
for i in range(10):
    print(correct_neg[2][i])
print('\nWrong')
for i in range(10):
    print(wrong_non[2][i])


print('\nFear:')
print('Correct')
for i in range(10):
    print(correct_neg[3][i])
print('\nWrong')
for i in range(10):
    print(wrong_non[3][i])


print('\nSurprise:')
print('Correct')
for i in range(10):
    print(correct_neg[4][i])
print('\nWrong')
for i in range(10):
    print(wrong_non[4][i])


print('\nDisgust:')
print('Correct')
for i in range(10):
    print(correct_neg[5][i])
print('\nWrong')
for i in range(10):
    print(wrong_non[5][i])

