import sys
import pickle

emotions = ['joy','sadness','anger','fear','surprise','disgust']

def getemo(hd, w):
    while w not in emotions:
        w = hd[w]

    return w


syn_file = open('corpus/wn-domains-3.2/wn-domains-2.0-20050210', 'r', encoding='utf8')

hier_file = open('corpus/wn-domains-3.2/wn-affect-1.1/a-hierarchy.xml', 'r', encoding='utf8')

hier_dict = {}

for line in hier_file:
    line = line.split()
    hier_dict[line[1].split('=')[1].replace('\"', '')] = line[2].split('=')[1].split("/>")[0].replace('\"', '')


wn_file = open('corpus/wn-domains-3.2/wn-affect-1.1/a-synsets.xml', 'r', encoding='utf8')

noun_list = False
adj_list = False
verb_list = False
adv_list = False

id_dict = {}
c = 0
whole = 0
for line in wn_file:

    if line == '<noun-syn-list>\n':
        noun_list = True

    elif line == '<adj-syn-list>\n':
        adj_list = True
        noun_list = False

    elif line == '<verb-syn-list>\n':
        verb_list = True
        adj_list = False

    elif line == '<adv-syn-list>\n':
        adv_list = True
        verb_list = False

    elif noun_list:
        try:
            line = line.split()
            id_dict[line[1].split('=')[1].replace('\"', '')] = getemo(hier_dict,line[2].split('=')[1].split("/>")[0].replace('\"', ''))
        except:
            continue

    elif adj_list or verb_list or adv_list:
        line = line.split()
        if line[2].split('=')[1].replace('\"', '') in id_dict.keys():
            id_dict[line[1].split('=')[1].replace('\"', '')] = line[2].split('=')[1].replace('\"', '')

with open('wordnet_syns.txt', 'wb') as f:
    pickle.dump(id_dict, f)