import pickle
import sys
import re
import numpy
import random
import unicodedata as ud
from nltk.corpus import wordnet as wn
from nltk.util import *
from nltk import probability

def keywithmaxval(d):
	v=list(d.values())
	k=list(d.keys())
	return k[v.index(max(v))]

intweet_text = [0] * 7
tweet_text = []
tweet_POS = []
tweet_class = []
tweet_correct = []
tweet_lexicons = {}
tweet_stemmed = []

hc = []
ac = []
sadc = []
fc = []
surc = []
dc = []

#get list input from the three text files
with open('tweet_text_new.txt', 'rb') as f:
	tweet_text  = pickle.load(f)
#	intweet_text[0] = pickle.load(f)
#	intweet_text[1] = pickle.load(f)
#	intweet_text[2] = pickle.load(f)
#	intweet_text[3] = pickle.load(f)
#	intweet_text[4] = pickle.load(f)
#	intweet_text[5] = pickle.load(f)
#	intweet_text[6] = pickle.load(f)

#tweet_text = intweet_text[0] + intweet_text[1] + intweet_text[2] + intweet_text[3] + intweet_text[4] + intweet_text[5] + intweet_text[6]


with open('tweet_POS_new.txt', 'rb') as f:
	tweet_POS = pickle.load(f)
#	intweet_text[0] = pickle.load(f)
#	intweet_text[1] = pickle.load(f)
#	intweet_text[2] = pickle.load(f)
#	intweet_text[3] = pickle.load(f)
#	intweet_text[4] = pickle.load(f)
#	intweet_text[5] = pickle.load(f)
#	intweet_text[6] = pickle.load(f)

#tweet_POS = intweet_text[0] + intweet_text[1] + intweet_text[2] + intweet_text[3] + intweet_text[4] + intweet_text[5] + intweet_text[6]



with open('tweet_classification_new.txt', 'rb') as f:
	tweet_class = pickle.load(f)
#	intweet_text[0] = pickle.load(f)
#	intweet_text[1] = pickle.load(f)
#	intweet_text[2] = pickle.load(f)
#	intweet_text[3] = pickle.load(f)
#	intweet_text[4] = pickle.load(f)
#	intweet_text[5] = pickle.load(f)
#	intweet_text[6] = pickle.load(f)

#tweet_class = intweet_text[0] + intweet_text[1] + intweet_text[2] + intweet_text[3] + intweet_text[4] + intweet_text[5] + intweet_text[6]

#tt = []
#tp = []
#tc = []

#with open('tweet_text.txt', 'rb') as f:
#    tt = pickle.load(f)

#with open('tweet_POS.txt', 'rb') as f:
#    tp = pickle.load(f)

#with open('tweet_classification.txt', 'rb') as f:
#    tc = pickle.load(f)

#tweet_text = tweet_text + tt
#tweet_POS = tweet_POS + tp
#tweet_class = tweet_class + tc

#preds = []
#happy_preds = []
#anger_preds = []
#sadness_preds = []
#fear_preds = []
#surprise_preds = []
#disgust_preds = []

#with open('predict.txt', 'rb') as f:
#	tweet_class = pickle.load(f)
#	preds = pickle.load(f)
#	happy_preds = pickle.load(f)
#	anger_preds = pickle.load(f)
#	sadness_preds = pickle.load(f)
#	fear_preds = pickle.load(f)
#	surprise_preds = pickle.load(f)
#	disgust_preds = pickle.load(f)

#epreds = []
#epreds.append(happy_preds)
#epreds.append(anger_preds)
#epreds.append(sadness_preds)
#epreds.append(fear_preds)
#epreds.append(surprise_preds)
#epreds.append(disgust_preds)


#correct = [0] * 2
#m = { v: 0 for v in range(6) }

#for i in range(len(tweet_class)):
#	j = 0
#	e = 0
#	emo = { v: 0 for v in range(6) }
#	emo[preds[i]] += 1
#	for k in range(6):
#		if epreds[k][i] == 0:
#			j += 1
#			e = k
#			emo[k] += 1
#
#	if j == 1:
#		if e == tweet_class[i]:
#			correct[0] += 1
#		else:
#			correct[1] += 1
#	elif j == 0:
#		if preds[i] == tweet_class[i]:
#			correct[0] += 1
#		else:
#			correct[1] += 1
#	else:
#		if emo[keywithmaxval(emo)] == 1:
#			a = 0
#			l = []
#			for b,d in emo.items():
#				if d == 1:
#					a += 1
#					l.append(b)
#
#			m[a] += 1
#			r = random.randint(0,a-1)
#			if l[r] == tweet_class[i]:
#				correct[0] += 1
#			else:
#				correct[1] += 1
#		elif emo[keywithmaxval(emo)] > 1:
#			if keywithmaxval(emo) == tweet_class[i]:
#				correct[0] += 1
#			else:
#				correct[1] += 1
#
#print(correct)

#sys.exit(0)

dist = [0] * 6

neg = ['hardly','neither','nobody','not','cannot','lack','nor','none','lacking','never','nothing','without','lacks','no','nowhere','didnt','havent','neednt','wasnt','darent','hadnt','isnt','oughtnt','wouldnt','aint','dont','mightnt','shant','cant','doesnt','havnt','mustnt','shouldnt' ]

nneg = 0
i = 0
for tweet in tweet_text:
	after_neg = False
	for w in tweet:
			if (w.lower() in neg) or (re.search(r'[a-zA-z]+[nN](\')[tT]', w) is not None):
				nneg += 1
				dist[tweet_class[i]] += 1
				break
	i += 1


happy_tweets = []
happy_POS = []
anger_tweets = []
anger_POS = []
sadness_tweets = []
sadness_POS = []
fear_tweets = []
fear_POS = []
surprise_tweets = []
surprise_POS = []
disgust_tweets = []
disgust_POS = []

for i in range(len(tweet_class)):
	if tweet_class[i] == 0:
		happy_tweets.append(tweet_text[i])
		happy_POS.append(tweet_POS[i])
	elif tweet_class[i] == 1:
		anger_tweets.append(tweet_text[i])
		anger_POS.append(tweet_POS[i])
	elif tweet_class[i] == 2:
		sadness_tweets.append(tweet_text[i])
		sadness_POS.append(tweet_POS[i])
	elif tweet_class[i] == 3:
		fear_tweets.append(tweet_text[i])
		fear_POS.append(tweet_POS[i])
	elif tweet_class[i] == 4:
		surprise_tweets.append(tweet_text[i])
		surprise_POS.append(tweet_POS[i])
	elif tweet_class[i] == 5:
		disgust_tweets.append(tweet_text[i])
		disgust_POS.append(tweet_POS[i])



with open('pmi_happy.txt', 'wb') as f:
	pickle.dump(happy_tweets, f)

with open('pmi_POS_happy.txt', 'wb') as f:
	pickle.dump(happy_POS, f)

with open('pmi_anger.txt', 'wb') as f:
	pickle.dump(anger_tweets, f)

with open('pmi_POS_anger.txt', 'wb') as f:
	pickle.dump(anger_POS, f)

with open('pmi_sadness.txt', 'wb') as f:
	pickle.dump(sadness_tweets, f)

with open('pmi_POS_sadness.txt', 'wb') as f:
	pickle.dump(sadness_POS, f)

with open('pmi_fear.txt', 'wb') as f:
	pickle.dump(fear_tweets, f)

with open('pmi_POS_fear.txt', 'wb') as f:
	pickle.dump(fear_POS, f)

with open('pmi_surprise.txt', 'wb') as f:
	pickle.dump(surprise_tweets, f)

with open('pmi_POS_surprise.txt', 'wb') as f:
	pickle.dump(surprise_POS, f)

with open('pmi_disgust.txt', 'wb') as f:
	pickle.dump(disgust_tweets, f)

with open('pmi_POS_disgust.txt', 'wb') as f:
	pickle.dump(disgust_POS, f)

#with open('tweet_text_new.txt', 'wb') as f:
#    pickle.dump(tweet_text, f)

#with open('tweet_POS_new.txt', 'wb') as f:
#    pickle.dump(tweet_POS, f)

#with open('tweet_classification_new.txt', 'wb') as f:
#    pickle.dump(tweet_class, f)
