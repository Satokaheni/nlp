# -*- coding: utf-8 -*-
import numpy as np
import PosTokenizer as pt
from nltk.util import *
from nltk import probability
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import enchant
from spellchecker import SpellChecker

import pickle
import math
import sys

stemmer = PorterStemmer()

tweet_text = []
tweet_POS = []
tweet_class = []

#0 = joy, 1 = anger, 2 = sadness, 3 = fear, 4 = surprise, 5 = disgust
emotions = [ 0, 1, 2, 3, 4, 5 ]
	
#get tweets
with open('tweet_text.txt', 'rb') as f:
	tweet_text = pickle.load(f)

#get classification of tweets
with open('tweet_classification.txt', 'rb') as f:
	tweet_class = pickle.load(f)
	
unigram_list = []
bigram_list = []
sw = stopwords.words('english')
sc = SpellChecker()
d = enchant.Dict('en_US')
	
for tweet in tweet_text:
	unigram_list.extend(tweet)
	bigram_list.extend(bigrams(tweet))

u_count = probability.FreqDist(unigram_list)
b_count = probability.FreqDist(bigram_list)

#only take unigrams, bigrams and trigrams that occur more than 3 times
lexicons = []
lexicons.extend([ k for (k,v) in u_count.items() if v > 1 ])
lexicons.extend([ k for (k,v) in b_count.items() if v > 1 ])

print('lexicons created')

#get count of how much each lexicon occurs in each classification
counts = { x : { y : 0 for y in emotions } for x in lexicons }

#get on how much each class is seen
class_count = { y : 0 for y in emotions }

c = 0
for tweet in tweet_text:
	if c % 10000 == 0:
		print(c)
		
	text = [ w.lower() for w in tweet if w.lower() not in sw ]
	for word in text:
		if word in lexicons:
			counts[word][tweet_class[c]] = counts[word][tweet_class[c]] + 1
			
	bi = bigrams(text)
	for word in bi:
		if word in lexicons:
			counts[word][tweet_class[c]] = counts[word][tweet_class[c]] + 1

	class_count[tweet_class[c]] = class_count[tweet_class[c]] + 1
	c = c + 1

print('count finished starting soa calculation')	
	
#calculate strength of association for each word to the emotion
soa = { x : { y : 0 for y in emotions } for x in lexicons }

total = sum(class_count.values())
top_bottom = 0
bottom_top = 0
for (word,class_dict) in counts.items():
	for (c,v) in class_dict.items(): 
		PMI_top = counts[word][c]
		PMI_bottom = sum(counts[word].values())*class_count[c] + .001
#		PMI_top = counts[word][c]*(total-class_count[c]) + .001
#		PMI_bottom = class_count[c]*(sum(counts[word].values())-counts[word][c]) + .001
		if not (PMI_top == 0) and not (PMI_bottom == 0):
			soa[word][c] = math.log(PMI_top/PMI_bottom,2)
		else:
			soa[word][c] = 0
		if PMI_top > PMI_bottom:
			top_bottom = top_bottom + 1
		if PMI_bottom > PMI_top:
			bottom_top = bottom_top + 1

print(top_bottom)
print(bottom_top)
		
neg_words = set()
#remove words with negative values
for word,class_dict in soa.items():
	sum = 0	
	for c,v in class_dict.items():
		sum = sum + v
	if sum == 0:
		neg_words.add(word)

for word in neg_words:
	del soa[word]	


with open('tweet_lexicon_new.txt', 'ab') as f:
	pickle.dump(soa, f)
