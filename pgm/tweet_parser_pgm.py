# -*- coding: utf-8 -*-
import unicodedata as ud
import re
import json
import sys
import pickle

ec = sys.stdout.encoding

tweets_text = []
tweets_class = []
tweets_POS = []
tweets_file = open(sys.argv[1], 'r', encoding='utf-8')
tags_file = open(sys.argv[2], 'r', encoding='utf-8')

#list of non word POS tags
non_word_tags = ['#', '@', '~', 'U', 'E', '$', ',']

#list of tags used to get the emotions joy, anger, sad, fear, surprise, disgust
emotion_tags = ['#microsoft', '#google', '#apple', '#religion', '#sexuality', '#finance', '#banking', '#government', '#nsa']

#check if hashtag is last part of tweet
def checkend(hashtag, count, tag_list):
	classify = True
	afterhash = False
	hashcount = count
	
	#check if on right tag
	for (word,pos) in tag_list:	
		if (not afterhash) and word == hashtag:
			if hashcount > 1:
				hashcount = hashcount - 1
			else:
				afterhash = True
		#if after the hashtag and the next word is not a actual grammar word
		elif afterhash and not(pos in non_word_tags):
			classify = False
		#if after the hashtag and the next word is an emotion tag
		elif afterhash and word.lower() in emotion_tags:
			classify = False
	
	if classify and not(hashtag.lower() in emotion_tags):
		classify = False

	return classify
	
#remove the classfication hashtag from the text
def removeClassification(hashtag, text, count, pos_tags):
	afterhash = False
	tags = []
	hash_text = []
	hashcount = count
	
	#remove tag from pos_tags
	for (word,pos) in pos_tags:
		if not afterhash:
			if word == hashtag:
				if hashcount > 1:
					tags.append((word,pos))
					hashcount = hashcount - 1
				else:
					afterhash = True
			else:
				tags.append((word,pos))
		else:
			tags.append((word,pos))
	
	hashcount = count
	afterhash = False		
	#remove word from string
	for word in text.split():
		if not afterhash:
			if word == hashtag:
				if hashcount > 1:
					hash_text.append(word)
					hashcount = hashcount -1
				else:
					afterhash = True
			else:
				hash_text.append(word)
		else:
			hash_text.append(word)
		
	hashtext = ' '.join(hash_text)
	
	return (hashtext, tags)
			
	
#remove links since not important
def removeLinks(text, tag):
	pos_tags = []
	for (word,pos) in tag:
		if pos != 'U':
			pos_tags.append((word,pos))

	tweet = []
	for w in text.split():
		if w[:4] != 'http':
			tweet.append(w)
			
	return (' '.join(tweet),pos_tags)

#generalize the names of users to @User
def removeUsers(text, tag):
	tweet = []
	for w in text.split():
		if w[0] != '@':
			tweet.append(w)
	
	tag = [ (word,pos) for (word,pos) in tag if pos != '@' ]

	return (' '.join(tweet),tag)

#get actual classification tag from 6 emotions
def getClassification(hashtag):
	if hashtag in ['microsoft', 'google', 'apple']:
		return 0
	elif hashtag in ['religion', 'sexuality']:
		return 1
	elif hashtag in ['finance', 'banking']:
		return 2
	elif hashtag in ['government', 'nsa']:
		return 3
	
#read tags input file
tweet_tag = []
tagged_tweets = []

#to keep up with which tweet we are on
count = 0

for line in tags_file:
	if(line != '\n'):
		tweet_tag.append(line)
	else:
		tagged_tweet = []
		for item in tweet_tag:
			split = item.split('\t')
			word, tag = split[0], split[1]
			word = ud.normalize('NFC', word)
			tag = ud.normalize('NFC', tag)
			tagged_tweet.append((word,tag))
		tweet_tag = []
		tagged_tweets.append(tagged_tweet)

tags_file.close()



#read tweets from input and format for classification
for line in tweets_file:
	try:
		tweet = json.loads(line)
		tweet['text'] = ud.normalize('NFC', tweet['text'])
		tweet['text'] = tweet['text'].encode(ec).decode('utf-8')
		tweet['text'] = tweet['text'].replace('[link removed] ', '')
		tweet['text'] = tweet['text'].replace('\n\n', ' ')
		tweet['text'] = tweet['text'].replace('\n', ' ')
		tweet['text'] = tweet['text'].replace('\r', ' ')
		if tweet['text'][:2] != 'RT':
			#increase count
			count = count + 1
			hashtags = [ h['text'].encode(ec).decode('utf-8') for h in tweet['entities']['hashtags'] ]
			hashtag_count = {}
			for hashtag in hashtags:
				#in case of multiples of the same hashtag
				if hashtag in hashtag_count:
					hashtag_count[hashtag] = hashtag_count[hashtag] + 1
				else:
					hashtag_count[hashtag] = 1
							
				#check if classification is correct otherwise not classified and throw away
				if checkend('#'+hashtag, hashtag_count[hashtag], tagged_tweets[count-1]):
					text, tags = removeClassification('#'+hashtag, tweet['text'], hashtag_count[hashtag], tagged_tweets[count-1])
					text, tags = removeLinks(text, tags)
					text, tags = removeUsers(text, tags)
					hashtag = getClassification(hashtag.lower())
					tweets_text.append(text)
					tweets_POS.append(tags)
					tweets_class.append(hashtag)
					break
			
			
		
	except:
		continue
	
tweets_file.close()
tags_file.close()


#re write the text of tweets as a list of words instead of just strings
tweets_text_wordlist = []

for tweet in tweets_text:
	tweets_text_wordlist.append(tweet.split())

#write data to files
with open('tweet_text_pgm.txt', 'wb') as f:
	pickle.dump(tweets_text_wordlist, f)

with open('tweet_POS_pgm.txt', 'wb') as f:	
	pickle.dump(tweets_POS, f)
	
with open('tweet_classification_pgm.txt', 'wb') as f:
	pickle.dump(tweets_class, f)
	
