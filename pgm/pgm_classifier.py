# -*- coding: utf-8 -*-
import numpy as np
from nltk.util import *
from nltk import probability
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import *
import pickle
import sys

tweet_text = []
tweet_POS = []
tweet_class = []

#get list input from the three text files
with open('tweet_text.txt', 'rb') as f:
	tweet_text = pickle.load(f)
	
with open('tweet_POS.txt', 'rb') as f:
	tweet_POS = pickle.load(f)
	
with open('tweet_classification.txt', 'rb') as f:
	tweet_class = pickle.load(f)	
	
#list of all possible POS tags
tag_list = ['N', 'O', 'S', '^', 'Z', 'L', 'M', 'V', 'A', 'R', '!', 'D', 'P', '&', 'T', 'X', 'Y', '#', '@', '~', 'E', '$', ',', 'G']	
	
#get list of all possible trigrams, bigrams, trigrams in the tweet corpus
#only use bigrams and trigrams that appear more than three times
#only use unigrams that appear more than 4 times
unigram_list = []
bigrams_list = []
trigrams_list = []

for tweet in tweet_text:
	text = [ w.lower() for w in tweet ]
	unigram_list.extend(ngrams(text, 1))
	bigrams_list.extend(bigrams(text))
	trigrams_list.extend(trigrams(text))

all_unigrams = probability.FreqDist(unigram_list)
unigram_features = dict((k,v) for k,v in all_unigrams.items() if v > 2)
all_bigrams = probability.FreqDist(bigrams_list)
bigram_features = dict((k,v) for k,v in all_bigrams.items() if v > 1)
all_trigrams = probability.FreqDist(trigrams_list)
trigram_features = dict((k,v) for k,v in all_trigrams.items() if v > 1)

#split data into training and testing
num_tweets = len(tweet_text)
training_size = int(num_tweets*(2/3))
test_size = int(num_tweets*(1/3))


#non baseline feature selection
def features(tweet_text, tweet_POS):
	tweet = [ w.lower() for w in tweet_text ]
	tweet_unigrams = set(tweet)
	tweet_bigrams = set(bigrams(tweet))
	tweet_trigrams = set(trigrams(tweet))
	features = []
	elongated_pmarks = 0
	elongated_words = 0
	
	#unigram features
	for word in unigram_features:
		if word in tweet_unigrams:
			features.append(1)
		else:
			features.append(0)
	
	#bigram features
	for word in bigram_features:
		if word in tweet_bigrams:
			features.append(1)
		else:
			features.append(0)
		
	#trigram features
	for word in trigram_features:
		if word in tweet_trigrams:
			features.append(1)
		else:
			features.append(0)
		
	
	return features

#actual classifier
clf = MultinomialNB()
actual_features = [features(tweet,tag) for (tweet,tag) in zip(tweet_text, tweet_POS)]
training_tweets = actual_features[:training_size]
training_class = tweet_class[:training_size]
testing_tweets = actual_features[-test_size:]
testing_class = tweet_class[-test_size:]			

#train the classifier
y_pred = clf.fit(training_tweets, training_class).predict(testing_tweets)

error = 0
for i in range(len(y_pred)):
	if y_pred[i] != testing_class[i]:
		error = error + 1
		
accuracy = 1 - error/len(y_pred)

print('Naive Bayes: ' + str(accuracy*100))

#SVM classifier
sclf = SVC()
y_pred = sclf.fit(training_tweets, training_class).predict(testing_tweets)

error = 0
for i in range(len(y_pred)):
	if y_pred[i] != testing_class[i]:
		error = error + 1
		
accuracy = 1 - error/len(y_pred)

print('SVM: ' + str(accuracy*100))