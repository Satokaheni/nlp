# -*- coding: utf-8 -*-
from nltk.util import *
import operator
import pickle
import sys
import random
from nltk.corpus import stopwords

tweets = []
tweet_class = []

its = int(sys.argv[1])
k_actual = int(sys.argv[2])

#get tweets from file
with open('tweet_text_pgm.txt', 'rb') as f:
	tweets= pickle.load(f)
	
with open('tweet_classification_pgm.txt', 'rb') as f:
	tweet_class = pickle.load(f)

topics = [0, 1, 2, 3]	
list_words = []
training_size = int(len(tweets)*(2/3))
tweet_text = tweets

#possible actual training
real_dist_list = []
c = 0
for tweet in tweet_text:
	tweet_word = []
	for word in tweet:
		tweet_word.append((word.lower(), tweet_class[c]))
	real_dist_list.append(tweet_word)
	c = c + 1
	
for tweet in tweet_text:
	tweet_word = []
	for word in tweet:
		t = random.sample(topics, 1)
		tweet_word.append((word.lower(), t[0]))
	list_words.append(tweet_word)

unigram_list = []
	
for tweet in tweet_text:
    text = [ w.lower() for w in tweet ]
    unigram_list.extend(text)	
	
vocab = len(unigram_list)	
unigram_list = set(unigram_list)	

a_dist = { w: { k : 0 for k in topics } for w in unigram_list }

#getting real distribution
for tweet in real_dist_list:
	for (w,t) in tweet:
		a_dist[w][t] = a_dist[w][t] + 1
		
for w in a_dist:
	norm = a_dist[w][0] + a_dist[w][1] + a_dist[w][2] + a_dist[w][3]
	for t in a_dist[w]:
		a_dist[w][t] = a_dist[w][t]/norm
		
	
#Gibbs sampler LDA
for i in range(its):
	p_td = []

	for tweet in list_words:
		topic = {key: 0 for key in topics}
		for (w,t) in tweet:
			topic[t] = topic[t] + 1
		topic = {key: (value/len(tweet)) for (key,value) in topic.items()}
		p_td.append(topic)
		
	p_tw = {key: { w: 0 for w in unigram_list } for key in topics}
	count = {key: 0 for key in topics}
	for tweet in list_words:
		for (w,t) in tweet:
			p_tw[t][w] = p_tw[t][w] + 1
			count[t] = count[t] + 1

	for t in topics:
		for (w,v) in p_tw[t].items():
			p_tw[t][w] = p_tw[t][w]/count[t]
	
	for t in topics:
		for (k,v) in p_tw[t].items():
			v = v/count[t]

	d = 0
	for tweet in list_words:
		for (w,t) in tweet:
			p = []
			a = 0
			for top in topics:
				p.append(p_td[d][top]*p_tw[top][w])
				a = a + p_td[d][top]*p_tw[top][w]
			
			for v in range(len(p)):
				p[v] = p[v]/a
			
			for v in range(len(p)):
				if v != 0:
					p[v] = p[v] + p[v-1]
			
			r = random.random()
			
			for v in range(len(p)):
				if r < p[v]:
					t = topics[v]
			
		d = d + 1		
		
	print('iteration: (%d)' % i)

tweets_prime = []
tweets_class_prime = []
lda_dist = { w: { k: 0 for k in topics } for w in unigram_list }
for tweet in list_words:
	tweet_dist = [0]*4
	for (w,t) in tweet:
		tweet_dist[t] = tweet_dist[t] + 1
		lda_dist[w][t] = lda_dist[w][t] + 1
	
	tweet_dist = [ v/len(tweet) for v in tweet_dist ] 
	tweets_prime.append(tweet_dist)
	max = tweet_dist[3]
	c = 3
	for i in range(3):
		if tweet_dist[i] > max:
			max = tweet_dist[i]
			c = i
	tweets_class_prime.append(c)
	
for w in lda_dist:
	n = lda_dist[w][0] + lda_dist[w][1] + lda_dist[w][2] + lda_dist[w][3]
	for t in lda_dist[w]:
		lda_dist[w][t] = lda_dist[w][t]/n
	
#a_dist	
a_tech = { w: t[0] for (w,t) in a_dist.items() }
a_rel = { w: t[1] for (w,t) in a_dist.items() }
a_fin = { w: t[2] for (w,t) in a_dist.items() }
a_gov = { w: t[3] for (w,t) in a_dist.items() }
a_tech = sorted(a_tech.items(), key=operator.itemgetter(1))[:k_actual]
a_rel = sorted(a_rel.items(), key=operator.itemgetter(1))[:k_actual]
a_fin = sorted(a_fin.items(), key=operator.itemgetter(1))[:k_actual]
a_gov = sorted(a_gov.items(), key=operator.itemgetter(1))[:k_actual]

d_tech = [0]*4
d_rel = [0]*4
d_fin = [0]*4

for (w,v) in a_tech:
	max_index = 3
	for i in range(3):
		if lda_dist[w][i] > lda_dist[w][max_index]:
			max_index = i
	d_tech[max_index] = d_tech[max_index] + 1
	
for (w,v) in a_rel:
	max_index = 3
	for i in range(3):
		if lda_dist[w][i] > lda_dist[w][max_index]:
			max_index = i
	d_rel[max_index] = d_rel[max_index] + 1
	
for (w,v) in a_fin:
	max_index = 3
	for i in range(3):
		if lda_dist[w][i] > lda_dist[w][max_index]:
			max_index = i
	d_fin[max_index] = d_fin[max_index] + 1
	
max_t = 3

taken = [False]*4
	
for i in range(4):
	if d_tech[i] > d_tech[max_t]:
		max_t = i

taken[max_t] = True

max_r = 4
for i in range(4):
	if not taken[i]:
		max_r = i
		break
		
for i in range(4):
	if d_rel[i] > d_rel[max_r] and not taken[i]:
		max_r = i

taken[max_r] = True
max_f = 4
for i in range(4):
	if not taken[i]:
		max_f = i
		break;
		
for i in range(4):
	if d_fin[i] > d_fin[max_f] and not taken[i]:
		max_f = i
		
taken[max_f] = True

max_g = 4
for i in range(4):
	if not taken[i]:
		max_g = i
	
mapping = { 0 : max_t, 1 : max_r, 2 : max_f, 3 : max_g }

error = 0

for i in range(len(tweet_text)):
	if mapping[tweet_class[i]] != tweets_class_prime[i]:
		error = error + 1
		
accuracy = error/len(tweets)

print(accuracy)