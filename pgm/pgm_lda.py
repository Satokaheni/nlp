# -*- coding: utf-8 -*-
from nltk.util import *
import pickle
import numpy as np
import lda

tweets = []
tweet_class = []

#get tweets from file
with open('tweet_text_pgm.txt', 'rb') as f:
	tweets = pickle.load(f)
	
tweets = tweets[:4000]	
	
all_words = set()

for t in tweets:
	tweet = [ w.lower() for w in t ]
	for word in tweet:
		all_words.add(word)

word_hash = {}

count = 0
for word in all_words:
	word_hash[word] = count
	count = count + 1	
	
na = []

for tweet in tweets:
	wc = [0]*len(all_words)
	na.append(wc)

X = np.array(na)
	
count = 0	
for t in tweets:
	tweet = [ w.lower() for w in t ]
	for word in tweet:
		wh = word_hash[word]
		X[count,wh] = X[count,wh] + 1
	count = count + 1
	
model = lda.LDA(n_topics=4, n_iter=500, random_state=1)
model.fit(X)

topic_word = model.topic_word_
print(topic_word.shape)